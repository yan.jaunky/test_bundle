<?php

namespace Pdg\ValidatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PdgValidatorBundle:Default:index.html.twig');
    }
}
